const inputField = document.querySelector('#token');
inputField.value = localStorage.getItem('token')
inputField.addEventListener("change", cacheToken);

    function cacheToken() {
        localStorage.setItem('token', inputField.value);
    }

    // a function to set the input field style based on the HTTP status code
    function setInputFieldStyle(status) {
        if (status === 200) {
            inputField.style.borderColor = 'green';
        } else {
            inputField.style.borderColor = 'red';
        }
    }

    function loadData()
    {
        const token = inputField.value;
        const headers = 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        };
    
        fetch('https://api.spotify.com/v1/me/player/currently-playing', {headers: headers})
        .then(response => {
            setInputFieldStyle(response.status);
            if (response.ok) {
                inputField.setAttribute
                return response.json();
            }
            else
            {
                document.querySelector("#cover").style.display = "none";
                document.querySelector("#check").style.display = "none";
                document.querySelector("#artist").style.display = "none";
                document.querySelector("#song").style.display = "none";
                document.querySelector("#release-year").style.display = "none";
            }

            throw new Error('Request failed.');
        })
        .then(data => {
            
            // an array to store the artists
            const artists = [];
            const cover = document.getElementById("cover");

            // iterate through the artists array in the response data
            data.item.artists.forEach(artist => {
                // add the artist name to the artists array
                artists.push(artist.name);
            });

            // add cover image
            if (typeof data !== "undefined" && data.item.album.images.length > 0) {
                const url = data.item.album.images[0].url;
                cover.innerHTML = `<img height="300px" src="${url}" alt="">`;
                cover.style.display = "inline";
            } else {
                cover.innerHTML = "";
            }

            const artist = document.querySelector('#artist');
            artist.textContent = artists.join(' / ');
            artist.style.display = "inline"; 

            const song = document.querySelector('#song');
            song.textContent = data.item.name;
            song.style.display = "inline"; 

            const releaseYear = document.querySelector('#release-year');
            releaseYear.textContent = data.item.album.release_date.split('-')[0];
            releaseYear.style.display = "inline"; 

            // set google check link
            const searchterm = artist.textContent + " - " + song.textContent + " release year";
            console.log(searchterm);
            var searchLink = "https://www.google.com/search?q=" + encodeURIComponent(searchterm);
            document.getElementById("check").href = searchLink;
            document.getElementById("check").style.display = "inline";
        
            // debugging
            console.log(data);
        
            const d = data;
        })
        .catch(error => {
            console.log(error);
        });
    }

    const button = document.querySelector('#load-button');
    button.addEventListener('click', loadData);